#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1

VERSION=`rpmspec -q --srpm --qf "%{version}" libthai.spec 2>/dev/null`

if test -d libthai-$VERSION-build;
then cd libthai-$VERSION-build;
fi

cd libthai-$VERSION

./configure --prefix=/usr
check_return_value $?
make
check_return_value $?
make check
exit $?
